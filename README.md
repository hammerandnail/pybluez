PyBluez
=======

Remove type check for MacOS, sending type should be bytes.

Using hard code to make discover_devices' lookup_names parm worked on MacOS.

Cloned from : https://github.com/pybluez/pybluez

```python
import bluetooth

bd_addr = '12:34:56:78:90:XX'
port = 1
sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.connect((bd_addr,port))

sock.send('Hello world!'.encode())

sock.close()
```

Now it is worked on my Macbook, hope it work on your's too.

#### macOS

-   Python 2.3 or later
-   Xcode
-   PyObjc 3.1b or later
    (<https://pythonhosted.org/pyobjc/install.html#manual-installation>)